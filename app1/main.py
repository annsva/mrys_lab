import os

def count_decimal_digits(number):
    # Функция для подсчета количества десятичных цифр в числе
    decimal_part = str(number).split(".")[1]
    return len(decimal_part)

def compare_decimal_digits(num1, num2):
    # Функция для сравнения количества десятичных цифр в двух числах
    count1 = count_decimal_digits(num1)
    count2 = count_decimal_digits(num2)

    if count1 > count2:
        return f"{num1} имеет больше десятичных цифр, чем {num2}"
    elif count1 < count2:
        return f"{num2} имеет больше десятичных цифр, чем {num1}"
    else:
        return f"Оба числа имеют одинаковое количество десятичных цифр: {count1}"

if __name__ == "__main__":
    # Получаем значения из переменных окружения, если они установлены, иначе используем значения по умолчанию
    num1 = float(os.environ.get("NUM1", 0.0))
    num2 = float(os.environ.get("NUM2", 0.0))

    result = compare_decimal_digits(num1, num2)
    print(result)