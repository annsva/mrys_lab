CREATE DATABASE IF NOT EXISTS  mrys_lab_db;
USE mrys_lab_db;
CREATE TABLE IF NOT EXISTS stud_group
(
    student_id INTEGER PRIMARY KEY,
    student_fn VARCHAR(50) NOT NULL,
    student_ln VARCHAR(50) NOT NULL,
    student_bd DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS record_book
(
    record_book_id serial PRIMARY KEY,
    student_id INTEGER NOT NULL,
    discipline VARCHAR(50) NOT NULL,
    exam_date VARCHAR(50) NOT NULL,
    teacher VARCHAR(50) NOT NULL,
    mark INTEGER NOT NULL,
    FOREIGN KEY (student_id) REFERENCES stud_group (student_id) ON DELETE CASCADE
);



INSERT IGNORE  INTO stud_group (student_id, student_fn, student_ln, student_bd)
VALUES
(1, 'Vasya', 'Ivanov', '2002-03-13'),
(2, 'Lera', 'Smirnova', '2002-06-02'),
(3, 'Nikita', 'Pupkin', '2003-01-09'),
(4, 'Anastsiya', 'Popova', '2002-12-23'),
(5, 'Anna', 'Abramova', '2002-09-29'),
(6, 'Vladimir', 'Vasiliev', '2002-07-19'),
(7, 'Yan', 'Danilov', '2001-11-17'),
(8, 'Marina', 'Petrova', '2002-08-07'),
(9, 'Daniil', 'Danilov', '2002-03-15'),
(10, 'Petr', 'Knyazev', '2002-10-01');

INSERT IGNORE  INTO record_book (student_id, discipline, exam_date, teacher, mark)
VALUES
(1,     'Informatics',  '2024-01-29',   'Potapov Makar',        5),
(1,     'Physics',      '2024-01-21',   'Osipova Karina',       3),
(1,     'History',      '2024-01-14',   'Gromova Vasilisa',     4),
(1,     'Math', '2024-01-08',   'Losev Gleb',   5),
(5,     'Math', '2024-01-08',   'Losev Gleb',   4),
(4, 'Math',     '2024-01-08',   'Losev Gleb',   3),
(3,     'Math', '2024-01-08',   'Losev Gleb',   4),
(2,     'Math', '2024-01-08',   'Losev Gleb',   4),
(6,     'Math', '2024-01-08',   'Losev Gleb', 5),
(7,     'Math', '2024-01-08',   'Losev Gleb',   3),
(8,     'Math', '2024-01-08',   'Losev Gleb',   2),
(9,     'Math', '2024-01-08',   'Losev Gleb',   4),
(10,    'Math', '2024-01-08',   'Losev Gleb',   5),
(2,     'History',      '2024-01-14',   'Gromova Vasilisa',     4),
(3,     'History',      '2024-01-14',   'Gromova Vasilisa',  4),
(4,     'History',      '2024-01-14',   'Gromova Vasilisa',     5),
(5,     'History',      '2024-01-14',   'Gromova Vasilisa',     4),
(6,     'History',      '2024-01-14',   'Gromova Vasilisa',     5),
(7,     'History',      '2024-01-14',   'Gromova Vasilisa',     5),
(8,     'History',      '2024-01-14',   'Gromova Vasilisa',     5),
(9,     'History',      '2024-01-14',   'Gromova Vasilisa'      ,4),
(10,    'History',      '2024-01-14',   'Gromova Vasilisa',     3),
(2,     'Physics',      '2024-01-21',   'Osipova Karina',       3),
(3,     'Physics',      '2024-01-21',   'Osipova Karina',       5),
(4,     'Physics',      '2024-01-21',   'Osipova Karina',       3),
(5,     'Physics',      '2024-01-21',   'Osipova Karina',       3),
(6,     'Physics',      '2024-01-21',   'Osipova Karina',       4),
(7,     'Physics',      '2024-01-21',   'Osipova Karina',       4),
(8,     'Physics',      '2024-01-21',   'Osipova Karina',       3),
(9,     'Physics',      '2024-01-21',   'Osipova Karina',       4),
(10,    'Physics'       ,'2024-01-21',  'Osipova Karina',       3),
(2,     'Informatics',  '2024-01-29',   'Potapov Makar',        5),
(3,     'Informatics',  '2024-01-29',   'Potapov Makar',        4),
(4,     'Informatics',  '2024-01-29',   'Potapov Makar',        3),
(5,     'Informatics',  '2024-01-29',   'Potapov Makar',        4),
(6,     'Informatics',  '2024-01-29',   'Potapov Makar',        4),
(7,     'Informatics',  '2024-01-29',   'Potapov Makar' ,5),
(8,     'Informatics',  '2024-01-29',   'Potapov Makar',        5),
(9,     'Informatics',  '2024-01-29',   'Potapov Makar',        5),
(10,    'Informatics',  '2024-01-29',   'Potapov Makar' ,4);