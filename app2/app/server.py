import mysql.connector

mydb = mysql.connector.connect(
    host="mysql-service",
    user="root",
    password="root",
    database="mrys_lab_db"
)

mycursor = mydb.cursor()
mycursor.execute("SELECT DISTINCT discipline, exam_date FROM record_book")
exam_data = mycursor.fetchall()

for row in exam_data:
    print(f'Предмет: {row[0]}, Экзамен: {row[1]}')